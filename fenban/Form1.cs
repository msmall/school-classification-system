﻿using Masuit.Tools.Hardware;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fenban
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void fenbanBtn_Click(object sender, EventArgs e)
        {
            string fenbanNum = fenbannum.Text;
            int banjinumu = int.Parse(fenbanNum);
            float load = SystemInfo.CpuLoad;
            //打开根目录下文件
            string path= System.Windows.Forms.Application.StartupPath;
            string excelpath = path + "/fenban.xlsx";
            DataTable dt = NPOIHelper.ExcelToTable(excelpath);
            int studentCount = dt.Rows.Count;
            double classNum =(double)studentCount /banjinumu;
            
            int stunum = (int)Math.Floor(classNum);
            int synum = int.Parse(zhengshu(classNum, false));
            int ncl= dt.Select("性别='男' and 来源='城区' and 能力='L'").Length;
            int ncc = dt.Select("性别='男' and 来源='城区' and 能力='C'").Length;
            int ncy = dt.Select("性别='男' and 来源='城区' and 能力='Y'").Length;
            int ncz = dt.Select("性别='男' and 来源='城区' and 能力='Z'").Length;
            int nxl = dt.Select("性别='男' and 来源='乡镇' and 能力='L'").Length;
            int nxc = dt.Select("性别='男' and 来源='乡镇' and 能力='C'").Length;
            int nxy = dt.Select("性别='男' and 来源='乡镇' and 能力='Y'").Length;
            int nxz = dt.Select("性别='男' and 来源='乡镇' and 能力='Z'").Length;
            int vcl = dt.Select("性别='女' and 来源='城区' and 能力='L'").Length;
            int vcc = dt.Select("性别='女' and 来源='城区' and 能力='C'").Length;
            int vcy = dt.Select("性别='女' and 来源='城区' and 能力='Y'").Length;
            int vcz = dt.Select("性别='女' and 来源='城区' and 能力='Z'").Length;
            int vxl = dt.Select("性别='女' and 来源='乡镇' and 能力='L'").Length;
            int vxc = dt.Select("性别='女' and 来源='乡镇' and 能力='C'").Length;
            int vxy = dt.Select("性别='女' and 来源='乡镇' and 能力='Y'").Length;
            int vxz = dt.Select("性别='女' and 来源='乡镇' and 能力='Z'").Length;
            //double nclc = (double)ncl / banjinumu;
            //int nclb= int.Parse(zhengshu(nclc));
            //int ncly= int.Parse(zhengshu(nclc,false));
            //double nccc = (double)ncc / banjinumu;
            //int nccb = int.Parse(zhengshu(nccc));
            //int nccy = int.Parse(zhengshu(nccc, false));
            //double ncyc = (double)ncy / banjinumu;
            //int ncyb = int.Parse(zhengshu(ncyc));
            //int ncyy = int.Parse(zhengshu(ncyc, false));
            //double nczc = (double)ncz / banjinumu;
            //int nczb = int.Parse(zhengshu(nczc));
            //int nczy = int.Parse(zhengshu(nczc, false));
            //double nxlc = (double)nxl / banjinumu;
            //int nxlb = int.Parse(zhengshu(nxlc));
            //int nxly = int.Parse(zhengshu(nxlc, false));
            //double nxcc = (double)nxc / banjinumu;
            //int nxcb = int.Parse(zhengshu(nxcc));
            //int nxcy = int.Parse(zhengshu(nxcc, false));
            //double nxyc = (double)nxy / banjinumu;
            //int nxyb = int.Parse(zhengshu(nxyc));
            //int nxyy = int.Parse(zhengshu(nxyc, false));
            //double nxzc = (double)nxz / banjinumu;
            //int nxzb = int.Parse(zhengshu(nxzc));
            //int nxzy = int.Parse(zhengshu(nxzc, false));
            //double vclc = (double)vcl / banjinumu;
            //int vclb = int.Parse(zhengshu(vclc));
            //int vcly = int.Parse(zhengshu(vclc, false));
            //double vccc = (double)vcc / banjinumu;
            //int vccb = int.Parse(zhengshu(vccc));
            //int vccy = int.Parse(zhengshu(vccc, false));
            //double vcyc = (double)vcy / banjinumu;
            //int vcyb = int.Parse(zhengshu(vcyc));
            //int vcyy = int.Parse(zhengshu(vcyc, false));
            //double vczc = (double)vcz / banjinumu;
            //int vczb = int.Parse(zhengshu(vczc));
            //int vczy = int.Parse(zhengshu(vczc, false));
            //double vxlc = (double)vxl / banjinumu;
            //int vxlb = int.Parse(zhengshu(vxlc));
            //int vxly = int.Parse(zhengshu(vxlc, false));
            //double vxcc = (double)vxc / banjinumu;
            //int vxcb = int.Parse(zhengshu(vxcc));
            //int vxcy = int.Parse(zhengshu(vxcc, false));
            //double vxyc = (double)vxy / banjinumu;
            //int vxyb = int.Parse(zhengshu(vxyc));
            //int vxyy = int.Parse(zhengshu(vxyc, false));
            //double vxzc = (double)vxz / banjinumu;
            //int vxzb = int.Parse(zhengshu(vxzc));
            //int vxzy = int.Parse(zhengshu(vxzc, false));
            double nclc = (double)ncl / banjinumu;
            int nclb = int.Parse(zhengshu(nclc));
            int ncly = ncl- banjinumu*nclb;
            double nccc = (double)ncc / banjinumu;
            int nccb = int.Parse(zhengshu(nccc));
            int nccy = ncc - banjinumu*nccb;
            double ncyc = (double)ncy / banjinumu;
            int ncyb = int.Parse(zhengshu(ncyc));
            int ncyy = ncy - banjinumu*ncyb;
            double nczc = (double)ncz / banjinumu;
            int nczb = int.Parse(zhengshu(nczc));
            int nczy = ncz - banjinumu*nczb;
            double nxlc = (double)nxl / banjinumu;
            int nxlb = int.Parse(zhengshu(nxlc));
            int nxly = nxl - banjinumu*nxlb;
            double nxcc = (double)nxc / banjinumu;
            int nxcb = int.Parse(zhengshu(nxcc));
            int nxcy = nxc - banjinumu* nxcb;
            double nxyc = (double)nxy / banjinumu;
            int nxyb = int.Parse(zhengshu(nxyc));
            int nxyy = nxy - banjinumu* nxyb;
            double nxzc = (double)nxz / banjinumu;
            int nxzb = int.Parse(zhengshu(nxzc));
            int nxzy = nxz - banjinumu* nxzb;
            double vclc = (double)vcl / banjinumu;
            int vclb = int.Parse(zhengshu(vclc));
            int vcly = vcl - banjinumu* vclb;
            double vccc = (double)vcc / banjinumu;
            int vccb = int.Parse(zhengshu(vccc));
            int vccy = vcc - banjinumu* vccb;
            double vcyc = (double)vcy / banjinumu;
            int vcyb = int.Parse(zhengshu(vcyc));
            int vcyy = vcy - banjinumu* vcyb;
            double vczc = (double)vcz / banjinumu;
            int vczb = int.Parse(zhengshu(vczc));
            int vczy = vcz - banjinumu* vczb;
            double vxlc = (double)vxl / banjinumu;
            int vxlb = int.Parse(zhengshu(vxlc));
            int vxly = vxl - banjinumu* vxlb;
            double vxcc = (double)vxc / banjinumu;
            int vxcb = int.Parse(zhengshu(vxcc));
            int vxcy = vxc - banjinumu* vxcb;
            double vxyc = (double)vxy / banjinumu;
            int vxyb = int.Parse(zhengshu(vxyc));
            int vxyy = vxy - banjinumu* vxyb;
            double vxzc = (double)vxz / banjinumu;
            int vxzb = int.Parse(zhengshu(vxzc));
            int vxzy = vxz - banjinumu*vxzb;
            //  List<ClassStudent> list = new List<ClassStudent> { };
            List<List<ClassStudent>> list = new List < List < ClassStudent>>{ };
            List <ClassStudent> ncllist = savelist(dt.Select("性别='男' and 来源='城区' and 能力='L'"));
            List<ClassStudent> ncclist = savelist(dt.Select("性别='男' and 来源='城区' and 能力='C'"));
            List<ClassStudent> ncylist = savelist(dt.Select("性别='男' and 来源='城区' and 能力='Y'"));
            List<ClassStudent> nczlist = savelist(dt.Select("性别='男' and 来源='城区' and 能力='Z'"));
            List<ClassStudent> nxllist = savelist(dt.Select("性别='男' and 来源='乡镇' and 能力='L'"));
            List<ClassStudent> nxclist = savelist(dt.Select("性别='男' and 来源='乡镇' and 能力='C'"));
            List<ClassStudent> nxylist = savelist(dt.Select("性别='男' and 来源='乡镇' and 能力='Y'"));
            List<ClassStudent> nxzlist = savelist(dt.Select("性别='男' and 来源='乡镇' and 能力='Z'"));
            List<ClassStudent> vcllist = savelist(dt.Select("性别='女' and 来源='城区' and 能力='L'"));
            List<ClassStudent> vcclist = savelist(dt.Select("性别='女' and 来源='城区' and 能力='C'"));
            List<ClassStudent> vcylist = savelist(dt.Select("性别='女' and 来源='城区' and 能力='Y'"));
            List<ClassStudent> vczlist = savelist(dt.Select("性别='女' and 来源='城区' and 能力='Z'"));
            List<ClassStudent> vxllist = savelist(dt.Select("性别='女' and 来源='乡镇' and 能力='L'"));
            List<ClassStudent> vxclist = savelist(dt.Select("性别='女' and 来源='乡镇' and 能力='C'"));
            List<ClassStudent> vxylist = savelist(dt.Select("性别='女' and 来源='乡镇' and 能力='Y'"));
            List<ClassStudent> vxzlist = savelist(dt.Select("性别='女' and 来源='乡镇' and 能力='Z'"));
            getStudent(banjinumu,nclb,ncllist,list, ncly);
            getStudent(banjinumu, nccb, ncclist, list,nccy);
            getStudent(banjinumu, ncyb, ncylist, list,ncyy);
            getStudent(banjinumu, nczb, nczlist, list, nczy);
            getStudent(banjinumu, nxlb, nxllist, list, nxly);
            getStudent(banjinumu, nxcb, nxclist, list, nxcy);
            getStudent(banjinumu, nxyb, nxylist, list, nxyy);
            getStudent(banjinumu, nxzb, nxzlist, list, nxzy);
            getStudent(banjinumu, vclb, vcllist, list, vcly);
            getStudent(banjinumu, vccb, vcclist, list, vccy);
            getStudent(banjinumu, vcyb, vcylist, list, vcyy);
            getStudent(banjinumu, vczb, vczlist, list, vczy);
            getStudent(banjinumu, vxlb, vxllist, list, vxly);
            getStudent(banjinumu, vxcb, vxclist, list, vxcy);
            getStudent(banjinumu, vxyb, vxylist, list, vxyy);
            getStudent(banjinumu, vxzb, vxzlist, list, vxzy);
            //for (int i=1; i<banjinumu+1;i++) {
            //    List<ClassStudent> lists = new List<ClassStudent> { };

            //    List<ClassStudent> listt= ncllist.Take(nclb).ToList();

            //    //foreach (var ll in listt) {
            //    //    ClassStudent classStudent = new ClassStudent();
            //    //    classStudent.班级1 = i + "班";
            //    //    classStudent.姓名1 = ll.姓名1;

            //    //    lists.Add(classStudent);


            //    //}
            //    for (int ii = 0; ii < listt.Count; ii++)
            //    {
            //        ClassStudent classStudent = new ClassStudent();
            //            classStudent.班级1 = i + "班";
            //           classStudent.姓名1 = listt[ii].姓名1;

            //            lists.Add(classStudent);
            //            ncllist.Remove(listt[ii]);
            //    }




            //    list.Add(lists);




            //}
            Console.WriteLine(JsonConvert.SerializeObject(list));
            Console.WriteLine(list);
            FileStream fs = File.Open(path+"\\分班结果.txt", FileMode.Create);

            // 创建写入流
            StreamWriter wr = new StreamWriter(fs);
            //list.Sort();

            DataTable ds = new DataTable();
            //1.创建空列
             
      
            //2.创建带列名和类型名的列(两种方式任选其一)
            ds.Columns.Add("班级", typeof(String));
            ds.Columns.Add("姓名", typeof(String));
            ds.Columns.Add("姓别", typeof(String));
            ds.Columns.Add("来源", typeof(String));
            ds.Columns.Add("能力", typeof(String));

            // 将ArrayList中的每个项逐一写入文件
            for (int i = 0; i < list.Count; i++)
            {
                // wr.WriteLine(list[i]);
                foreach (var ll in list[i].OrderBy(o=>o.班级1)) {
                    DataRow dr = ds.NewRow();
                   //3.通过行框架创建并赋值
                    ds.Rows.Add(ll.班级1, ll.姓名1, ll.性别1, ll.来源1, ll.能力1);//Add里面参数的数据顺序要和dt中的列的顺序对应 
                                                    //4.通过复制dt2表的某一行来创建
                 
                    wr.WriteLine(ll.姓名1+ll.班级1+ll.来源1+ll.能力1+ll.性别1);
                    
                }
            }
            string excelpaths = path + "\\分班结果.xls";
            NPOIHelper.ExportByServer(ds, "分班结果", excelpaths);
            // 关闭写入流
            wr.Flush();
            wr.Close();

            // 关闭文件
            fs.Close();

        }
  
        private void getStudent(int banjinumu, int nclb, List<ClassStudent> ncllist, List<List<ClassStudent>>list,int ncly)
        {


      
            for (int i = 1; i < banjinumu + 1; i++)
            {
                List<ClassStudent> lists = new List<ClassStudent> { };
                List<ClassStudent> listt = ncllist.Take(nclb).ToList();



                for (int ii = 0; ii < listt.Count; ii++)
                {
                    ClassStudent classStudent = new ClassStudent();
                    classStudent.班级1 = i + "班";
                    classStudent.姓名1 = listt[ii].姓名1;
                    classStudent.性别1 = listt[ii].性别1;
                    classStudent.来源1 = listt[ii].来源1;
                    classStudent.能力1 = listt[ii].能力1;



                    lists.Add(classStudent);
                   // listt.Remove(listt[ii]);
                    ncllist.Remove(listt[ii]);
                   //
                }
                list.Add(lists);
                 

            }

            if (ncly > 0)
            {
                for (int a = 1; a < ncly + 1; a++)
                {
                    List<ClassStudent> lists = new List<ClassStudent> { };
                    ClassStudent classStudent1 = new ClassStudent();
                    classStudent1.班级1 = a + "班";
                    classStudent1.姓名1 = ncllist[0].姓名1;
                    classStudent1.性别1 = ncllist[0].性别1;
                    classStudent1.来源1 = ncllist[0].来源1;
                    classStudent1.能力1 = ncllist[0].能力1;
                    lists.Add(classStudent1);
                    ncllist.Remove(ncllist[0]);
                    list.Add(lists);
                }

              
            }
           
        }

        //将List转换为TXT文件 

        public void WriteListToTextFile(List<string> list, string txtFile)

        {

            //创建一个文件流，用以写入或者创建一个StreamWriter 

            FileStream fs = new FileStream(txtFile, FileMode.OpenOrCreate, FileAccess.Write);

            StreamWriter sw = new StreamWriter(fs);

            sw.Flush();

            // 使用StreamWriter来往文件中写入内容 

            sw.BaseStream.Seek(0, SeekOrigin.Begin);

            for (int i = 0; i < list.Count; i++) sw.WriteLine(list[i]);

            //关闭此文件 

            sw.Flush();

            sw.Close();

            fs.Close();

        }

    
    private List<ClassStudent> savelist(DataRow[] dr) {
            List<ClassStudent> list = new List<ClassStudent> { };
            foreach (var row in dr) {
                ClassStudent classStudent = new ClassStudent();
                classStudent.姓名1 = row["姓名"].ToString();
                classStudent.性别1 = row["性别"].ToString();
                classStudent.来源1 = row["来源"].ToString();
                classStudent.能力1 = row["能力"].ToString();
                list.Add(classStudent);
            }
            return list;
           


        }
        private string zhengshu(double val,bool zhegnshu=true){
            string vals = val.ToString();
            string[] split = vals.Split(new Char[] { '.' });
            if (zhegnshu)
            {
                return split[0];
            }
            else {
                 string shengyu = "";
                try
                {
                    return split[1].Substring(0, 1);
                }
                catch (Exception e) {
                    return "0";
                }
            }
           
        }
    }
}
