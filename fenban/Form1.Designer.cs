﻿
namespace fenban
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.fenbanBtn = new System.Windows.Forms.Button();
            this.fenbannum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fenbanBtn
            // 
            this.fenbanBtn.Location = new System.Drawing.Point(217, 85);
            this.fenbanBtn.Name = "fenbanBtn";
            this.fenbanBtn.Size = new System.Drawing.Size(104, 32);
            this.fenbanBtn.TabIndex = 0;
            this.fenbanBtn.Text = "分班";
            this.fenbanBtn.UseVisualStyleBackColor = true;
            this.fenbanBtn.Click += new System.EventHandler(this.fenbanBtn_Click);
            // 
            // fenbannum
            // 
            this.fenbannum.Location = new System.Drawing.Point(117, 85);
            this.fenbannum.Name = "fenbannum";
            this.fenbannum.Size = new System.Drawing.Size(71, 21);
            this.fenbannum.TabIndex = 1;
            this.fenbannum.Text = "8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "分班";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "班";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fenbannum);
            this.Controls.Add(this.fenbanBtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fenbanBtn;
        private System.Windows.Forms.TextBox fenbannum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

